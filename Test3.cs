using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;

namespace SeleniumTests
{
    [TestFixture]
    public class Test3
    {
        private IWebDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;
        
        [SetUp]
        public void SetupTest()
        {
            driver = new FirefoxDriver();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }
        
        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            Assert.AreEqual("", verificationErrors.ToString());
        }
        
        [Test]
        public void The3Test()
        {
            driver.Navigate().GoToUrl("https://demo.opencart.com/index.php?route=checkout/cart");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='$ US Dollar'])[1]/following::span[2]")).Click();
            driver.FindElement(By.XPath("//body")).Click();
            driver.FindElement(By.LinkText("Your Store")).Click();
            driver.FindElement(By.LinkText("Show All Laptops & Notebooks")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Ex Tax: $100.00'])[1]/following::span[1]")).Click();
            driver.FindElement(By.Id("cart-total")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Wish List (0)'])[1]/following::span[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='product 11'])[1]/following::i[2]")).Click();
            driver.FindElement(By.LinkText("Continue")).Click();
            driver.FindElement(By.LinkText("Laptops & Notebooks")).Click();
            driver.FindElement(By.LinkText("Show All Laptops & Notebooks")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Ex Tax: $100.00'])[1]/following::span[1]")).Click();
            driver.FindElement(By.Id("button-cart")).Click();
            driver.FindElement(By.Id("cart-total")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Shopping Cart'])[1]/following::span[1]")).Click();
            driver.FindElement(By.Id("input-payment-firstname")).Click();
            driver.FindElement(By.Id("input-payment-firstname")).Clear();
            driver.FindElement(By.Id("input-payment-firstname")).SendKeys("Barbara");
            driver.FindElement(By.Id("input-payment-lastname")).Click();
            driver.FindElement(By.Id("input-payment-lastname")).Clear();
            driver.FindElement(By.Id("input-payment-lastname")).SendKeys("Matijevic");
            driver.FindElement(By.Id("input-payment-company")).Click();
            driver.FindElement(By.Id("input-payment-company")).Click();
            driver.FindElement(By.Id("input-payment-company")).Click();
            driver.FindElement(By.Id("input-payment-company")).Click();
            // ERROR: Caught exception [ERROR: Unsupported command [doubleClick | id=input-payment-company | ]]
            driver.FindElement(By.Id("input-payment-address-1")).Click();
            driver.FindElement(By.Id("input-payment-address-1")).Click();
            driver.FindElement(By.Id("input-payment-address-1")).Click();
            // ERROR: Caught exception [ERROR: Unsupported command [doubleClick | id=input-payment-address-1 | ]]
            driver.FindElement(By.Id("input-payment-address-1")).Clear();
            driver.FindElement(By.Id("input-payment-address-1")).SendKeys("K. Tomislava 4");
            driver.FindElement(By.Id("input-payment-city")).Click();
            driver.FindElement(By.Id("input-payment-city")).Clear();
            driver.FindElement(By.Id("input-payment-city")).SendKeys("Osijek");
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Show All MP3 Players'])[1]/following::ul[1]")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Your Store'])[1]/following::button[2]")).Click();
            driver.FindElement(By.XPath("//body")).Click();
            driver.FindElement(By.Id("input-payment-country")).Click();
            new SelectElement(driver.FindElement(By.Id("input-payment-country"))).SelectByText("Croatia");
            driver.FindElement(By.Id("input-payment-zone")).Click();
            new SelectElement(driver.FindElement(By.Id("input-payment-zone"))).SelectByText("Grad Zagreb");
            driver.FindElement(By.Id("input-payment-zone")).Click();
            driver.FindElement(By.Id("button-payment-address")).Click();
            driver.FindElement(By.Id("button-shipping-address")).Click();
            driver.FindElement(By.Name("comment")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Your Store'])[1]/following::i[2]")).Click();
            driver.FindElement(By.Name("comment")).Click();
            driver.FindElement(By.Id("button-shipping-method")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Comments About Your Order'])[2]/following::textarea[1]")).Click();
            driver.FindElement(By.Id("button-payment-method")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Comments About Your Order'])[2]/following::textarea[1]")).Click();
            driver.FindElement(By.Name("agree")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Add Comments About Your Order'])[2]/following::textarea[1]")).Click();
            driver.FindElement(By.Id("button-payment-method")).Click();
            driver.FindElement(By.Id("button-confirm")).Click();
            driver.FindElement(By.Id("content")).Click();
            driver.FindElement(By.Id("content")).Click();
            driver.FindElement(By.Id("content")).Click();
            driver.FindElement(By.LinkText("Continue")).Click();
            driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='$ US Dollar'])[1]/following::span[2]")).Click();
            driver.FindElement(By.LinkText("Logout")).Click();
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
        
        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }
        
        private string CloseAlertAndGetItsText() {
            try {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert) {
                    alert.Accept();
                } else {
                    alert.Dismiss();
                }
                return alertText;
            } finally {
                acceptNextAlert = true;
            }
        }
    }
}
